jQuery(function($){
	$("input[type=phone]").mask("+7 (999) 999-9999");
});

"use strict";

var form = document.querySelector('.candidate__form');
var emailInput = form.querySelector('.form__field-input--mail');
var errorsContainer = form.querySelector('.form__errors');
var formErrors = {
	EMAIL: 'поле email заполнено неверно',
	FORM: 'форма заполнена неверно'
};

form.addEventListener('submit', checkValidity);
emailInput.addEventListener('change', checkEmailValidity);

// Валидируем всю форму
function checkValidity(evt) {
	evt.preventDefault();
	if (!form.checkValidity()) {
		appendErrorInDOM('FORM');
	}
	return null;
}

// Валидируем отдельный инпут
function checkEmailValidity() {
	if (emailInput.checkValidity()) {
		return null;
	}
	appendErrorInDOM('EMAIL');
}

// Вставляем в дом сообщение об ошибке
function appendErrorInDOM(type) {
	var errorBox = document.createElement('p');
	console.log('', formErrors[type]);
	errorBox.innerHTML = formErrors[type];
	errorsContainer.appendChild(errorBox);
}



